import React from 'react'
const Event = props => <div>
    <h2>
        {props.name} ({props.location})
    </h2>
    <p>
        {props.startDate}-{props.endDate}
    </p>
    <p>
        website: <a href={props.url}>{props.url}</a>
    </p>
</div>
export default Event