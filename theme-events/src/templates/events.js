import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import LayOut from "../components/layout";
import EventList from "../components/events-list";
const EventsTemplate = () => {
  const data = useStaticQuery(graphql`
    query {
      allEvent(sort: { fields: startDate, order: ASC }) {
        nodes {
          id
          name
          startDate
          endDate
          location
          url
          slug
        }
      }
    }
  `);
  const events = data.allEvent.nodes;
  return (
    <LayOut>
      <EventList events={events} />
    </LayOut>
  );
};

export default EventsTemplate;
